#!/usr/bin/python

#
# -debug to show debugging
#

import sys
import MySQLdb as mdb
import os
import time
from colorama import init
init(strip=not sys.stdout.isatty()) # strip colors if stdout is redirected
from termcolor import cprint, colored
from pyfiglet import figlet_format
#from datetime import datetime
from dateutil import parser
import subprocess

version = "1.4"
update = "2015-06-09"
path = "/usr/local/oiremote"
mysql_host = 'localhost'
mysql_user = 'server'
mysql_pass = 'Mysql Oxford Instruments'
mysql_db = 'remote'
mysql_key = '/etc/mysql/client-key.pem'
mysql_cert = '/etc/mysql/client-cert.pem'
mysql_ca = '/etc/mysql/ca-cert.pem'
mysql_capath = None
mysql_cipher = 'DHE-RSA-AES256-SHA'
ssl = {'cert': mysql_cert, 'key': mysql_key, 'ca': mysql_ca, 'capath': mysql_capath, 'cipher': mysql_cipher}
mycon = None
mycur = None
debug = None
ping_good_max = 150 #<= this num
ping_poor_max = 200 #<= this num > ping_good
#ping bad is greater than ping poor max

#
# Connect to mysql server local
#
def mysql_connect():
	global mycon, mycur, ssl
	try:
		mycon = mdb.connect(host=mysql_host, user=mysql_user, passwd=mysql_pass, db=mysql_db, ssl=ssl)
		mycur = mycon.cursor(mdb.cursors.DictCursor)
		return True
	except mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)

#
# Close mysql connection local
#
def mysql_close():
	global mycon
	if mycon:
		mycon.close()

def box(dir,count):
	dirs = {'t':u"\u2550",
			'b':u"\u2550",
			'tl':u"\u2554",
			'tr':u"\u2557",
			'bl':u"\u255A",
			'br':u"\u255D",
			's':u"\u2551"}
	out = ""
	for i in range(0,count):
		 out += dirs[dir]
	return out

#
# Clear screen
#
def clear():
	os.system('clear')

#
# Program header
#
def header():
	clear()
	cprint(figlet_format('O i R e m o t e', font='slant'),'cyan')
	print "Version: " + version 
	print "Release Date: " + update
	if debug:
		print "DEBUGGING MODE"
	print ""

#
# Main Menu
#
def show_main_menu():
	header()
	print box('tl',1) + box('t',1) + colored(" Main Menu ",'cyan', attrs=['bold']) + box('t',50) + box('tr',1)
	print box('s',1) + "".ljust(62) + box('s',1)
	
	print box('s',1) + "   " + colored("F",'red', attrs=['bold']) + " - Find System ID by System Name".ljust(58) + box('s',1)

	print box('s',1) + "".ljust(62) + box('s',1)
	print box('s',1) + "   " + colored("S",'red', attrs=['bold']) + " - Show connection status for system".ljust(58) + box('s',1)

	print box('s',1) + "".ljust(62) + box('s',1)
	print box('s',1) + "   " + colored("U",'red', attrs=['bold']) + " - Update online system list".ljust(58) + box('s',1)

	print box('s',1) + "".ljust(62) + box('s',1)
	print box('s',1) + "   " + colored("Q",'red', attrs=['bold']) + " - Quit".ljust(58) + box('s',1)

	print box('s',1) + "".ljust(62) + box('s',1)
	print box('s',1) + "".ljust(62) + box('s',1)
	print box('s',1) + "".ljust(62) + box('s',1)
	print box('bl',1) + box('b',62) + box('br',1)
	print ""
	sys.stdout.write("\033[3A")
	sys.stdout.write("\033[3C")
	sys.stdout.write(colored('Type a menu item or enter system id: ','cyan',attrs=['bold']))
	rtn = raw_input()
	return rtn

#
# Find site menu
#
def show_find_menu():
	global mycur, mycon
	while True:
		header()	
		print box('tl',1) + box('t',1) + colored(" Find System ID ",'cyan', attrs=['bold']) + box('t',45) + box('tr',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "   " + colored("Type the name of the system and press enter",'cyan', attrs=['bold']).ljust(72) + box('s',1)
		print box('s',1) + "   " + colored("Like:high field",'cyan', attrs=['bold']).ljust(72) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "   " + colored("A max of 10 results will be returned",'cyan', attrs=['bold']).ljust(72) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)	
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "   " + colored("B",'red', attrs=['bold']) + " - Back to main menu".ljust(58) + box('s',1)
	
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('bl',1) + box('b',62) + box('br',1)
		print ""
		sys.stdout.write("\033[3A")
		sys.stdout.write("\033[3C")
		sys.stdout.write(colored('Search or Menu item: ','cyan',attrs=['bold']))
		rtn = raw_input()
		if rtn.lower() == 'b':
			return 0

		header()		
		sql =  "SELECT system_id, nickname FROM remote_computers WHERE nickname LIKE '%{0}%' LIMIT 10;".format(rtn)
		if debug:
			print "\n" + sql + "\n"
			
		if mysql_connect():
			mycur.execute(sql)
			rows = mycur.fetchall()
			if debug:
				print ""
				print rows
				print ""
				
			print box('tl',1) + box('t',1) + colored(" Lookup System ",'cyan', attrs=['bold']) + box('t',46) + box('tr',1)
			print box('s',1) + "".ljust(62) + box('s',1)
			for row in rows:
				print box('s',1) + "   " + row['system_id'] + " - " + row['nickname'][:42].ljust(52) + box('s',1)	
			print box('s',1) + "".ljust(62) + box('s',1)
			print box('s',1) + "".ljust(62) + box('s',1)
			print box('s',1) + "".ljust(62) + box('s',1)
			print box('bl',1) + box('b',62) + box('br',1)
			print ""
			sys.stdout.write("\033[3A")
			sys.stdout.write("\033[3C")
		else:
			raw_input("Error Occured!!! Press any key to return to the main menu")

		mysql_close()
		raw_input(colored("Press any key to return to the main menu",'cyan',attrs=['bold']))

		return 0

					
		


#
# Show status of site
#
def show_status_menu():
	while True:
		header()	
		print box('tl',1) + box('t',1) + colored(" Remote Status ",'cyan', attrs=['bold']) + box('t',46) + box('tr',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		
		print box('s',1) + "   " + colored("Type the System ID and press enter",'cyan', attrs=['bold']).ljust(72) + box('s',1)
		print box('s',1) + "   " + colored("Like:1234",'cyan', attrs=['bold']).ljust(72) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "   " + colored("B",'red', attrs=['bold']) + " - Back to main menu".ljust(58) + box('s',1)
	
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('s',1) + "".ljust(62) + box('s',1)
		print box('bl',1) + box('b',62) + box('br',1)
		print ""
		sys.stdout.write("\033[3A")
		sys.stdout.write("\033[3C")
		sys.stdout.write(colored('System ID or Menu item: ','cyan',attrs=['bold']))
		rtn = raw_input()
		if rtn.lower() == 'b':
			return 0
				
		header()		
		sql = """SELECT rc.*, ro.ping_avg, ro.`date`
				FROM remote_computers AS rc
				LEFT JOIN remote_online AS ro ON rc.vpn_mac = ro.vpn_mac
				WHERE rc.system_id = '{0}'""".format(rtn)
		if debug:
			print "\n" + sql + "\n"
			
		if mysql_connect():
			mycur.execute(sql)
			row = mycur.fetchone()
			if debug:
				print ""
				print row
				print ""

			print box('tl',1) + box('t',1) + colored(" Remote Status ",'cyan', attrs=['bold']) + box('t',46) + box('tr',1)
			print box('s',1) + "".ljust(62) + box('s',1)

			if row is not None:
				print box('s',1) + "   Status: Online".ljust(62) + box('s',1)	
				print box('s',1) + "".ljust(62) + box('s',1)					
				print box('s',1) + "   Facility Name: {0}".format(row['nickname'][:42]).ljust(62) + box('s',1)	
				print box('s',1) + "   System Name: {0}".format(row['system_name'][:42]).ljust(62) + box('s',1)	
				print box('s',1) + "   System Type: {0}".format(row['system_type']).ljust(62) + box('s',1)					
				print box('s',1) + "".ljust(62) + box('s',1)					
			
				if row['date'] == None:
					print box('s',1) + colored("   Remote Offline",'red').ljust(71) + box('s',1)
				else:
					if debug:
						print ""
						print "Date Data: " +  str(row['date'])
						print ""
					try:
						dt = row['date'].split('_')
						dt = dt[0].replace('-','/') + ' ' + dt[1].replace('-',':') + " +" + dt[2]
						dt = parser.parse(dt)
					except (IndexError, AttributeError):
						try:
							dt = row['date']
							dt = parser.parse(dt)
						except:
							pass
											
					print box('s',1) + "   Last Update: {0}".format(dt.strftime('%B %d, %Y %H:%M %Z')).ljust(62) + box('s',1)	
					
					try:
						ping = int(float(row['ping_avg']))
						if ping <= ping_good_max:
							print box('s',1) + "   Ping Time: " + colored(row['ping_avg'] + " Good Connection",'green').ljust(57) + box('s',1)	
						elif ping_good_max <= ping <= ping_poor_max:
							print box('s',1) + "   Ping Time: " + colored(row['ping_avg'] + " Poor connection",'yellow').ljust(57) + box('s',1)	
						elif ping > ping_poor_max:
							print box('s',1) + "   Ping Time: " + colored(row['ping_avg'] + " Very Bad Connection",'red').ljust(57) + box('s',1)	
							
					except:
						pass
					
					print box('s',1) + "".ljust(62) + box('s',1)
					print box('s',1) + "   System IP: {0}".format(row['system_ip']).ljust(62) + box('s',1)					
					print box('s',1) + "   System Port: {0}".format(row['system_port']).ljust(62) + box('s',1)					
					print box('s',1) + "   OiRemote IP: {0}".format(row['vpn_ip']).ljust(62) + box('s',1)								
					if row['connection_type'].lower() == 's':
						conn_type = "SSH   "
					else:
						conn_type = "Telnet"
					print box('s',1) + "   Connection Type: {0}".format(conn_type).ljust(62) + box('s',1)								
					print box('s',1) + "".ljust(62) + box('s',1)
					if row['notes'] is not None:
						notes = row['notes'].splitlines(False)
						print box('s',1) + "   Notes:".ljust(62) + box('s',1)					
						for line in notes:
							print box('s',1) + "   " + line[:42].ljust(59) + box('s',1)					
		
			else:
				print box('s',1) + "   System ID does not exist or is offline.".ljust(62) + box('s',1)	
				

			print box('s',1) + "".ljust(62) + box('s',1)
			print box('s',1) + "".ljust(62) + box('s',1)
			print box('s',1) + "".ljust(62) + box('s',1)
			print box('bl',1) + box('b',62) + box('br',1)
			print ""
			sys.stdout.write("\033[3A")
			sys.stdout.write("\033[3C")
		else:
			raw_input("Error Occured!!! Press any key to return to the main menu")

		mysql_close()
		raw_input(colored("Press any key to return to the main menu",'cyan',attrs=['bold']))

		return 0

	


#
# Show connect menu and connect
#
def show_connect_menu(id):
		while True:
			header()
			cnct = False		
			sql = "SELECT rc.*, ro.ping_min, ro.ping_avg, ro.ping_max, ro.`date` FROM remote_computers AS rc LEFT JOIN remote_online AS ro ON rc.vpn_mac = ro.vpn_mac WHERE rc.system_id = '{0}'".format(id)
			if debug:
				print sql
				print ""
			if mysql_connect():
				mycur.execute(sql)
				row = mycur.fetchone()
				if debug:
					print row
					print ""
				print box('tl',1) + box('t',1) + colored(" Connect To System ",'cyan', attrs=['bold']) + box('t',42) + box('tr',1)
				print box('s',1) + "".ljust(62) + box('s',1)
	
				if row is not None:
					cnct = True
					print box('s',1) + "   Status: Online".ljust(62) + box('s',1)	
					print box('s',1) + "".ljust(62) + box('s',1)					
					print box('s',1) + "   Facility Name: {0}".format(row['nickname'][:42]).ljust(62) + box('s',1)	
					print box('s',1) + "   System Name: {0}".format(row['system_name'][:42]).ljust(62) + box('s',1)	
					print box('s',1) + "   System Type: {0}".format(row['system_type']).ljust(62) + box('s',1)					
					print box('s',1) + "".ljust(62) + box('s',1)					
					
					if row['date'] == None:
						print box('s',1) + colored("   Remote Offline",'red').ljust(71) + box('s',1)
						cnct = False
					else:
						try:
							dt = row['date'].split('_')
							dt = dt[0].replace('-','/') + ' ' + dt[1].replace('-',':') + " +" + dt[2]
							dt = parser.parse(dt)
						except (IndexError, AttributeError):
							try:
								dt = row['date']
								dt = parser.parse(dt)
							except:
								pass
		
						print box('s',1) + "   Last Update: {0}".format(dt.strftime('%B %d, %Y %H:%M %Z')).ljust(62) + box('s',1)	
						
						try:
							ping = int(float(row['ping_avg']))
							if ping <= ping_good_max:
								print box('s',1) + "   Ping Time: " + colored(row['ping_avg'] + " Good Connection",'green').ljust(57) + box('s',1)	
							elif ping_good_max <= ping <= ping_poor_max:
								print box('s',1) + "   Ping Time: " + colored(row['ping_avg'] + " Poor connection",'yellow').ljust(57) + box('s',1)	
							elif ping > ping_poor_max:
								print box('s',1) + "   Ping Time: " + colored(row['ping_avg'] + " Very Bad Connection",'red').ljust(57) + box('s',1)	
								
						except:
							pass
						
						print box('s',1) + "".ljust(62) + box('s',1)
						print box('s',1) + "   System IP: {0}".format(row['system_ip']).ljust(62) + box('s',1)					
						print box('s',1) + "   System Port: {0}".format(row['system_port']).ljust(62) + box('s',1)					
						print box('s',1) + "   OiRemote IP: {0}".format(row['vpn_ip']).ljust(62) + box('s',1)								
						if row['connection_type'].lower() == 's':
							conn_type = "SSH   "
						else:
							conn_type = "Telnet"
						print box('s',1) + "   Connection Type: {0}".format(conn_type).ljust(62) + box('s',1)								
						print box('s',1) + "".ljust(62) + box('s',1)
						if row['notes'] is not None:
							notes = row['notes'].splitlines(False)
							print box('s',1) + "   Notes:".ljust(62) + box('s',1)					
							for line in notes:
								print box('s',1) + "   " + line.ljust(59) + box('s',1)					
				else:
					print box('s',1) + "   System ID does not exist or is offline.".ljust(62) + box('s',1)	
					
	
				if cnct:
					print box('s',1) + "".ljust(62) + box('s',1)
					print box('s',1) + "   " + colored("C",'red', attrs=['bold']) + " - Connect to site".ljust(58) + box('s',1)

				print box('s',1) + "".ljust(62) + box('s',1)
				print box('s',1) + "   " + colored("B",'red', attrs=['bold']) + " - Return to main menu".ljust(58) + box('s',1)
								
				print box('s',1) + "".ljust(62) + box('s',1)
				print box('s',1) + "".ljust(62) + box('s',1)
				print box('s',1) + "".ljust(62) + box('s',1)
				print box('bl',1) + box('b',62) + box('br',1)
				print ""
				sys.stdout.write("\033[3A")
				sys.stdout.write("\033[3C")
				sys.stdout.write(colored('Menu item or enter to retun to main menu: ','cyan',attrs=['bold']))
				mysql_close()
				rtn = raw_input()
				if rtn.lower() == 'b':
					return 0
				if cnct:
					if rtn.lower() == 'c':
						connect(row['vpn_ip'], row['system_ip'], row['system_port'], row['system_uid'], row['connection_type'])
						return 0
				if rtn.lower() == '':
					return 0
								
			else:
				raw_input("MySQL Error Occured!!! Press any key to return to the main menu")
		




#
# Connect to site
#
def connect(vip, sip, sport, suid, typ):
	header()

	if typ.lower() == 's':
		var = subprocess.call("ssh -A -t service@" + vip + " -p 3000 ssh " + suid + "@" + sip + " -p " + sport, shell=True)
	else:
		var = subprocess.call("ssh -A -t service@" + vip + " -p 3000 telnet -l" + suid + " " + sip, shell=True)
		
	print "Exit Code: {0}".format(var)
	if var == 255:
		raw_input("Connection Error Occured!!! Press any key to return to the main menu")
	else:
		raw_input("Connection Closed. Press any key to return to the main menu")
		
		
#
# Update online remote computers
#
def update_online_remotes():
	header()
	var = subprocess.call(path + "/./get_online_remotes.py")
	print "Exit Code: {0}".format(var)
	var = subprocess.call(path + "/./remote_computers_sync.py")
	print "Exit Code: {0}".format(var)
	raw_input("Press any key to return to the main menu")

#
# Sync users
#
def sync_users():
	header()
	var = subprocess.call(path + "/./sync_users.py")
	print "Exit Code: {0}".format(var)
	raw_input("Press any key to return to the main menu")
	


#
# Main Code
#
if __name__ == "__main__":
	f_menu_opts = ['f','find']
	s_menu_opts = ['s','status']
	u_menu_opts = ['u','update']
	x_menu_opts = ['x']
	q_menu_opts = ['q','quit']
	rtn = None
	arg = None
	try:	
		arg = sys.argv[1]
	except IndexError:
		pass
	if arg == '-debug':
		debug = True
	
	while True:
		if rtn is not None:
			if rtn.lower() in f_menu_opts:
				show_find_menu()
			elif rtn.lower() in s_menu_opts:
				show_status_menu()
			elif rtn.lower() in u_menu_opts:
				update_online_remotes()
			elif rtn.lower() in x_menu_opts:
				sync_users()
			elif rtn.lower() in  q_menu_opts:
				clear()
				sys.exit(0)
			else:
				try:
					val = int(rtn)
					show_connect_menu(rtn)
				except ValueError:
					pass			
			rtn = show_main_menu()
		else:
			rtn = show_main_menu()
