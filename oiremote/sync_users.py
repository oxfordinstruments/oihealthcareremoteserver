#!/usr/bin/python
# Created By Evotodi
# 10/7/2014

# Imports
import MySQLdb as mdb
import sys
import pwd
import grp
import os

# Vars Dynamic
mysql_host = 'oihealthcareportal.com'
mysql_user = 'oiheal5_web'
mysql_pass = 'Plat1num'
mysql_db = 'oiheal5_report'
mysql_key = '/etc/mysql/client-key.pem'
mysql_cert = '/etc/mysql/client-cert.pem'
mysql_ca = '/etc/mysql/ca-cert.pem'
mysql_capath = None
mysql_cipher = 'DHE-RSA-AES256-SHA'
path = '/usr/local/oiremote/'
jail_path = '/home/jail'

# Vars Static
mycon = None
mycur = None
ssl = {'cert': mysql_cert, 'key': mysql_key, 'ca': mysql_ca, 'capath': mysql_capath, 'cipher': mysql_cipher}
sql_users = dict()
loc_users = list()
add_users = list()
del_users = list()
upd_users = list()

#
# Check for root
#
def check_root():
	if not os.geteuid() == 0:
		sys.exit('Script must be run as root')

#
# Connect to mysql server
#
def mysql_connect():
	global mycon, mycur, ssl
	try:
		mycon = mdb.connect(host=mysql_host, user=mysql_user, passwd=mysql_pass, db=mysql_db, ssl=ssl)
		mycur = mycon.cursor(mdb.cursors.DictCursor)
		return True
	except mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)
		
#
# Close mysql connection
#
def mysql_close():
	global mycon
	if mycon:
		mycon.close()

#		
# Get the next highest local uid available	
#
def next_uid():
	uid = 2000
	for p in pwd.getpwall():
		if 2000 <= p[2] <= 65000:
			if p[2] > uid:
				uid = p[2]
	uid += 1			
	return uid

#
# Get users from sql db and store in sql_users
#
def get_sql_users():
	global sql_users
	if mysql_connect():
		mycur.execute("SELECT u.uid, u.linux_pwd, u.firstname, u.lastname FROM users AS u WHERE u.active = 'y' AND u.perm_remote_diag = 'y';")
		rows = mycur.fetchall()
		for row in rows:
			decrypted = os.popen("echo " + row['linux_pwd'] + "  | openssl enc -aes-256-cbc -d -a -nosalt -K 5A5731354F385447494D395452575941 -iv 524A374D4F3454325844555044564244").read()
			if len(decrypted) <= 46:
				tmp_pwd = decrypted
			else:
				tmp_pwd = False
			sql_users[row['uid']] = {'pwd': tmp_pwd, 'firstname': row['firstname'], 'lastname': row['lastname']}
	mysql_close()

#
# Get users from local sys and store in loc_users
#
def get_loc_users():
	global loc_users
	for p in pwd.getpwall():
		if 2000 <= p[2] <= 65000:
			loc_users.append(p[0])

#	
# Populate upd_users and add_users and del_users
#
def populate_upd_add_del():
	global sql_users
	global add_users
	global del_users
	global loc_users
	global upd_users
	
	for sql_user in sql_users:
		if sql_user in loc_users:
			upd_users.append(sql_user)
		else:
			add_users.append(sql_user)
		
	for loc_user in loc_users:
		if loc_user not in sql_users:
			del_users.append(loc_user)
			
	print "upd_users: ",upd_users,"\n"
	print "add_users: ",add_users,"\n"	
	print "del_users: ",del_users,"\n"

#
# Update Local Users
#
def loc_upd_users():
	global upd_users
	global sql_users
	global path
		
	for upd_user in upd_users:
		output = os.popen(path + "./sync_users-update.exp " + upd_user + " " + sql_users[upd_user]['pwd']).read()
		#print output
		print "Updated ",upd_user
		
#
# Add Local Users
#
def loc_add_users():
	global add_users
	global sql_users
	global path
	
	for add_user in add_users:
		uid = next_uid()
		output = os.popen(path + "./sync_users-add.exp " + add_user + " " + sql_users[add_user]['pwd'] + " " + str(uid) + " " + sql_users[add_user]['firstname'] + " " + sql_users[add_user]['lastname']).read()
		#print output
		output = os.popen(path + "./jailuser.sh " + add_user).read()
		print output
		print "Added ",add_user
	return False
		
#
# Delete local users
#
def loc_del_users():
	for del_user in del_users:
		output = os.popen("deluser --remove-home " + del_user).read()
		#print output
		output = os.popen(path + "./unjailuser.sh " + del_user).read()
		print output
		print "Removed ",del_user
	return False



#
# Main Program
#
if __name__ == '__main__':
	check_root()
	get_sql_users()
	get_loc_users()
	populate_upd_add_del()
	loc_upd_users()
	loc_add_users()
	loc_del_users()


                
