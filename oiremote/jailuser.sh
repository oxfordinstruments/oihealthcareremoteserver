#!/bin/bash

JAILPREFIX=/home/jail
HOMEDIR=/home

USERNAME=$1

[ "$1" == "" ] && {
        echo "Syntax: ${0} <username>"
        exit 1
}

jk_jailuser -v -m -j $JAILPREFIX -s /bin/bash $USERNAME

#sed -e 's|$/usr/sbin/jk_lsh|$/bin/bash|' $JAILPREFIX/etc/passwd > $JAILPREFIX/etc/passwd

echo "Done"
exit 0

