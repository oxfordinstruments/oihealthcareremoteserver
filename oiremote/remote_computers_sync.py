#!/usr/bin/python

import sys
import MySQLdb as mdb
import os
import time

path = "/usr/local/oiremote"
tmp = "/tmp"
mysql_host_local = 'localhost'
mysql_user_local = 'server'
mysql_pass_local = 'Mysql Oxford Instruments'
mysql_db_local = 'remote'
mysql_host_remote = 'oihealthcareportal.com'
mysql_user_remote = 'oiheal5_remote'
mysql_pass_remote = 'Plat1num'
mysql_db_remote = 'oiheal5_report'
mysql_key = '/etc/mysql/client-key.pem'
mysql_cert = '/etc/mysql/client-cert.pem'
mysql_ca = '/etc/mysql/ca-cert.pem'
mysql_capath = None
mysql_cipher = 'DHE-RSA-AES256-SHA'
ssl = {'cert': mysql_cert, 'key': mysql_key, 'ca': mysql_ca, 'capath': mysql_capath, 'cipher': mysql_cipher}
mycon_local = None
mycur_local = None
mycon_remote = None
mycur_remote = None
dt = time.strftime("%Y-%m-%dT%H:%M:%SZ")


#
# Check for root
#
def check_root():
	if not os.geteuid() == 0:
		sys.exit('Script must be run as root')

#
# Connect to mysql server local
#
def mysql_connect_local():
	global mycon_local, mycur_local, ssl
	try:
		mycon_local = mdb.connect(host=mysql_host_local, user=mysql_user_local, passwd=mysql_pass_local, db=mysql_db_local, ssl=ssl)
		mycur_local = mycon_local.cursor(mdb.cursors.DictCursor)
		return True
	except mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)

#
# Connect to mysql server remote
#
def mysql_connect_remote():
	global mycon_remote, mycur_remote, ssl
	try:
		mycon_remote = mdb.connect(host=mysql_host_remote, user=mysql_user_remote, passwd=mysql_pass_remote, db=mysql_db_remote, ssl=ssl)
		mycur_remote = mycon_remote.cursor(mdb.cursors.DictCursor)
		return True
	except mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)

#
# Close mysql connection local
#
def mysql_close_local():
	global mycon_local
	if mycon_local:
		mycon_local.close()

#
# Close mysql connection remote
#
def mysql_close_remote():
	global mycon_remote
	if mycon_remote:
		mycon_remote.close()

#
# Copy remote computer database from remote to local db
#
def copy_remote_db():
	print "Updating the local database..."
	if mysql_connect_local() and mysql_connect_remote():
		print "Clearing local db..."
		mycur_local.execute("TRUNCATE `remote_computers`;")
		mycon_local.commit()
		
		print "Getting remote table rows..."
		sql_remote = """SELECT rc.vpn_mac, rc.vpn_ip, rc.eth0_ip, ra.system_ip, ra.system_port, ra.connection_type, ra.system_uid, rc.uptime, rc.`version`, s.system_id, sbc.nickname,
						e.`type` AS system_type, e.name AS system_name,rc.reported_online, ra.notes
						FROM remote_assignments AS ra
						LEFT JOIN remote_computers AS rc ON rc.eth0_mac = ra.remote_serial
						LEFT JOIN systems_base AS s ON ra.system_unique_id = s.unique_id
						LEFT JOIN systems_base_cont AS sbc ON sbc.unique_id = s.unique_id AND property = 'C'
						LEFT JOIN systems_types AS e ON s.system_type = e.id
						WHERE remote_serial != '';"""
		mycur_remote.execute(sql_remote)
		rows = mycur_remote.fetchall()
		
		print "Inserting new local rows..."
		i = 0
		for row in rows:
			i += 1
			fields = ""
			values = ""
			for field, value in row.items():
				fields += "`" + field + "`, "

				if value is None:
					values += "NULL, "
				else:
					values += "'" + str(value) + "', "
				
			fields = fields[:-2]
			values = values[:-2]
			sql_local = "INSERT INTO remote_computers (" + fields + ") VALUES (" + values + ");"
			mycur_local.execute(sql_local)
			mycon_local.commit()

		print "{0} rows inserted".format(i)
		mysql_close_local()
		mysql_close_remote()
		return 1
	else:
		return 0

#
# Main Code
#
if __name__ == "__main__":
	print "Starting sync remote computers."
	check_root()
	if copy_remote_db() == 1:
		print "Complete"
		sys.exit(0)
	else:
		sys.exit(1)
