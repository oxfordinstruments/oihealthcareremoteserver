#!/usr/bin/python

#
# -debug to show debugging output
#

import subprocess
import shlex
import sys
import re
import MySQLdb as mdb
import os
import time
import csv

path = "/usr/local/oiremote"
tmp = "/tmp"
results_tmp = tmp + "/oi_ping_results.txt"
dhcp_tmp = tmp + "/oi_dhcp_table.csv"
ping_tmp = tmp + "/oi_ping_list.txt"
online_remotes = dict()
dhcp = dict()
softether_pw = "OxfordInstruments"
mysql_host_local = 'localhost'
mysql_user_local = 'server'
mysql_pass_local = 'Mysql Oxford Instruments'
mysql_db_local = 'remote'
mysql_host_remote = 'oihealthcareportal.com'
mysql_user_remote = 'oiheal5_remote'
mysql_pass_remote = 'Plat1num'
mysql_db_remote = 'oiheal5_report'
mysql_key = '/etc/mysql/client-key.pem'
mysql_cert = '/etc/mysql/client-cert.pem'
mysql_ca = '/etc/mysql/ca-cert.pem'
mysql_capath = None
mysql_cipher = 'DHE-RSA-AES256-SHA'
ssl = {'cert': mysql_cert, 'key': mysql_key, 'ca': mysql_ca, 'capath': mysql_capath, 'cipher': mysql_cipher}
mycon_local = None
mycur_local = None
mycon_remote = None
mycur_remote = None
dt = time.strftime("%Y-%m-%dT%H:%M:%SZ")
csv_hdr_ip = "Allocated IP"
csv_hdr_mac = "MAC Address"
debug = None


#
# Check for root
#
def check_root():
	if not os.geteuid() == 0:
		sys.exit('Script must be run as root')

#
# Connect to mysql server local
#
def mysql_connect_local():
	global mycon_local, mycur_local, ssl
	try:
		mycon_local = mdb.connect(host=mysql_host_local, user=mysql_user_local, passwd=mysql_pass_local, db=mysql_db_local, ssl=ssl)
		mycur_local = mycon_local.cursor(mdb.cursors.DictCursor)
		return True
	except mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)

#
# Connect to mysql server remote
#
def mysql_connect_remote():
	global mycon_remote, mycur_remote, ssl
	try:
		mycon_remote = mdb.connect(host=mysql_host_remote, user=mysql_user_remote, passwd=mysql_pass_remote, db=mysql_db_remote, ssl=ssl)
		mycur_remote = mycon_remote.cursor(mdb.cursors.DictCursor)
		return True
	except mdb.Error, e:
		print "Error %d: %s" % (e.args[0], e.args[1])
		sys.exit(1)

#
# Close mysql connection local
#
def mysql_close_local():
	global mycon_local
	if mycon_local:
		mycon_local.close()

#
# Close mysql connection remote
#
def mysql_close_remote():
	global mycon_remote
	if mycon_remote:
		mycon_remote.close()

#
# Gets the remotes that are online and returns the ping times and mac
#
def get_online_remotes():
	global online_remotes, ping_tmp, dhcp
	if get_dhcp_table() == 0:
		print "Getting online remote stats..."
	
		p_returncode = -1
		results_count = 0
		cmd = path + "/./fping.sh " + ping_tmp 
		args = shlex.split(cmd)
		if debug:
			print args
		with open(results_tmp,"wb") as err:
			p = subprocess.Popen(args,stdout=subprocess.PIPE,stderr=err)
			p.wait()
			if debug:
				print "fping return code: {0}".format(p.returncode)
			p_returncode = p.returncode
		
		if p_returncode <= 1:
			results_file = open(results_tmp,"r")
			for result in results_file.readlines():
				data = {"ip":None, "mac":None, "mam":None}
				if debug:
					print "file line {0}".format(result)
				if re.search('min/avg/max',result):
					results_count += 1
					if debug:
						print "match: {0}".format(result)
						print "IP Addy: {0}".format(result.split(' ',1)[0])
					data['ip'] = result.split(' ',1)[0].strip()
					match = re.search(r'[^= ]+(?:\d*\.)?\d+(\n|\n\r|\r)',result)
					if match:
						if debug:
							print "min/avg/max: {0}".format(match.group())
						mam = match.group().strip().split('/')
						data['mam'] = mam					
					
					if data['ip'] in dhcp:
						data['mac'] = dhcp[data['ip']]
					if debug:
						print "DATA: {0}".format(data)
					online_remotes[results_count - 1] = data
			if debug:
				print "get_online_remotes return " + str(results_count)
			return results_count
		else:
			if debug:
				print "get_online_remotes return " + str(results_count)
			return -1
	else:
		if debug:
			print "get_online_remotes return " + str(results_count)
		return -1
#
# Update the local database with the new info
#
def update_db_local():
	global dt
	print "Updating the local database..."
	if mysql_connect_local():
		mycur_local.execute("TRUNCATE `remote_online`;")
		mycon_local.commit()
		for orm in online_remotes:
			if debug:
				print online_remotes[orm]
			vpnip = online_remotes[orm]['ip']
			vpnmac = online_remotes[orm]['mac']
			min = online_remotes[orm]['mam'][0]
			avg = online_remotes[orm]['mam'][1]
			max = online_remotes[orm]['mam'][2]
			sql="""INSERT INTO remote_online 
				(vpn_ip, vpn_mac, ping_min, ping_avg, ping_max, `date`) 
				VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}') 
				ON DUPLICATE KEY UPDATE 
				vpn_mac = VALUES(vpn_mac), ping_min = VALUES(ping_min), 
				ping_avg = VALUES(ping_avg), ping_max = VALUES(ping_max), `date` = VALUES(`date`);"""
			if debug:
				print sql
			mycur_local.execute(sql.format(vpnip,vpnmac,min,avg,max,dt))
			mycon_local.commit()
	
		mysql_close_local()
		return 1
	else:
		return 0

#
# Update the remote database with the new info
#
def update_db_remote():
	global dt
	print "Updating the remote database..."
	if mysql_connect_remote():
		mycur_remote.execute("TRUNCATE `remote_online`;")
		mycon_remote.commit()
		for orm in online_remotes:
			if debug:
				print online_remotes[orm]
			vpnip = online_remotes[orm]['ip']
			vpnmac = online_remotes[orm]['mac']
			min = online_remotes[orm]['mam'][0]
			avg = online_remotes[orm]['mam'][1]
			max = online_remotes[orm]['mam'][2]
	
			sql="""INSERT INTO remote_online (vpn_ip, vpn_mac, ping_min, ping_avg, ping_max, `date`) 
				VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}') ON DUPLICATE KEY 
				UPDATE vpn_mac = VALUES(vpn_mac), ping_min = VALUES(ping_min), ping_avg = VALUES(ping_avg), 
				ping_max = VALUES(ping_max), `date` = VALUES(`date`);""".format(vpnip,vpnmac,min,avg,max,dt)
			if debug:
				print sql
			mycur_remote.execute(sql)
			#mycur_remote.execute("INSERT INTO remote_online (vpn_ip, vpn_mac, ping_min, ping_avg, ping_max, `date`) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}') ON DUPLICATE KEY UPDATE vpn_mac = VALUES(vpn_mac), ping_min = VALUES(ping_min), ping_avg = VALUES(ping_avg), ping_max = VALUES(ping_max), `date` = VALUES(`date`);".format(vpnip,vpnmac,min,avg,max,dt))
			mycon_remote.commit()
	
		mysql_close_remote()
		return 1
	else:
		return 0
	


#
#Gets the csv of the dhcp table from the softether server
#
def get_dhcp_table():
	global dhcp_tmp, dhcp
	print "Getting server dhcp table..."
	cmd = "/usr/local/vpnserver/./vpncmd localhost:1194 /SERVER /PASSWORD:{1} /ADMINHUB:oxford /OUT:{0} /CSV /CMD DhcpTable".format(dhcp_tmp,softether_pw)
	args = shlex.split(cmd)
	if debug:
		print args
	p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	output, errors = p.communicate()
	p.wait()
	p_returncode = p.returncode
	if debug:
		print errors
		print output
		print "softether returncode: {0}".format(p_returncode)
	if p_returncode == 0:
		with open(dhcp_tmp) as csv_file:
			reader = csv.DictReader(csv_file, delimiter=',')
			ip_list = open(ping_tmp,'w')
			for line in reader:
				dhcp[line[csv_hdr_ip]] = line[csv_hdr_mac].replace('-',':')
				ip_list.write(line[csv_hdr_ip] + "\n")
			ip_list.close()
			if debug:
				print dhcp
	return p_returncode
	
#
# Clean up temp files
#
def cleanup_tmp():
	print "Cleaning up files..."
	try:
		subprocess.Popen(["rm " + tmp + "/oi_*"], shell=True, stdout=subprocess.PIPE).communicate()
	except OSError:
		print "OS Error. Clean up failed."
	except:
		print "Clean up failed."


#
# Main Code
#
if __name__ == "__main__":
	print "Starting online remotes sync."
	arg = None
	try:
		arg = sys.argv[1]
	except IndexError:
		pass
	if arg == '-debug':
		debug = True
	check_root()
	if get_online_remotes() >= 0:
		if update_db_local() == 1:
			if update_db_remote() == 1:
				cleanup_tmp()
				print "Complete"
				sys.exit(0)
			else:
				sys.exit("Update DB Remote Failed")
		else:
			sys.exit("Update DB Local Failed")
	else:
		sys.exit("No remotes found")
	
