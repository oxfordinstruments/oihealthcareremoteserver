#!/bin/bash

JAIL=/home/jail
HOMEDIR=/home

USERNAME=$1

[ "$1" == "" ] && {
        echo "Syntax: ${0} <username>"
        exit 1
}

sed -i /$USERNAME/d $JAIL/etc/passwd
sed -i  "s/"$USERNAME"\|,"$USERNAME"\|"$USERNAME",//g" $JAIL/etc/group
rm -rf $JAIL/home/$USERNAME
echo "Done"
exit 0

